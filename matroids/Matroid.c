#include "Matroid.h"

struct Matroid* createMatroid(void* pS, void* pI){
    struct Matroid* matroid = malloc(sizeof (struct Matroid));
    matroid->S = pS;
    matroid->I = pI;
    return matroid;
}


void solveMatroid(struct Matroid* pMatroid, MatroidW pW){
    struct VectorOfPointers* I = (struct VectorOfPointers*)pMatroid->I;
    struct VectorOfPointers* S = (struct VectorOfPointers*)pMatroid->S;
    #pragma omp parallel for
    for (int elementPos = 0; elementPos < S->lenght; elementPos++) {
        void* element = S->pointers[elementPos];
        if(pW(element)){
            #pragma omp critical
            pushPointer(I, element);
        }
    }
}

