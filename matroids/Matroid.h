#ifndef MATROID
#define MATROID
#include <stdlib.h>

#include "vectorOfPointers.h"

typedef char (MatroidW)(void *);


struct Matroid{
    void *S;
    void *I;
};

struct Matroid* createMatroid(void* S, void* I);

void solveMatroid(struct Matroid* matroid, MatroidW W);

#endif
