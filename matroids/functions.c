#include "functions.h"

char isOdd(void* pNumber){
    return (*(int*)pNumber)&1;
}

char isEven(void* pNumber){
    return !((*(int*)pNumber)&1);
}

char isPrime(void* pNumber){
    int number = (*(int*)pNumber);
    if(number < 2){
        return 0;
    }
    for(int i = 2; i <= number/2; ++i){
        if(number%i == 0){
            return 0;
        }
    }
    return 1;
}

int* newInt(int number){
    int* pointer = malloc(sizeof (int));
    *pointer = number;
    return pointer;
}
