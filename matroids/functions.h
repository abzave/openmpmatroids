#ifndef FUNCTIONS_H
#define FUNCTIONS_H


char isOdd(void* number);

char isEven(void* number);

char isPrime(void* number);

int* newInt(int number);

#endif // FUNCTIONS_H
