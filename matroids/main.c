#include <stdio.h>
#include <omp.h>
#include <time.h>
#include "Matroid.h"
#include "functions.h"

#define INTERSECTIONS_TOTAL 5

void printIntMatroid(struct Matroid* pMatroid){
    printf("Matroid: {\n  S set: [");
    struct VectorOfPointers* S = (struct VectorOfPointers*)pMatroid->S;
    struct VectorOfPointers* I = (struct VectorOfPointers*)pMatroid->I;
    for (int element = 0; element < S->lenght-1; element++) {
        printf("%d,", *(int*)S->pointers[element]);
    }
    printf("%d]\n  I set: [",*(int*)S->pointers[S->lenght-1]);
    for (int element = 0; element < I->lenght-1; element++) {
        printf("%d,", *(int*)I->pointers[element]);
    }
    printf("%d]\n}\n", *(int*)I->pointers[I->lenght-1]);
}

struct VectorOfPointers* intersectIntMatroids(struct Matroid** pMatroids, int pSize){
    struct VectorOfPointers* intersection = (struct VectorOfPointers*)pMatroids[0]->I;
    for (int matroidIndex = 1; matroidIndex < pSize; matroidIndex++) {
        struct VectorOfPointers* currentI = (struct VectorOfPointers*)pMatroids[matroidIndex]->I;
        intersection = intersectIntVectors(intersection, currentI);
    }
    return intersection;
}

void testMatroid(int pRangeBegin, int pRangeEnd, MatroidW* pW){
    struct VectorOfPointers* S = newVectorOfPointers();
    struct VectorOfPointers* I = newVectorOfPointers();
    for (int i = pRangeBegin; i < pRangeEnd; i++) {
         pushPointer(S, newInt(i));
    }
    struct Matroid* matroid = createMatroid(S, I);
    solveMatroid(matroid, pW);
    printIntMatroid(matroid);
    free(S);
    free(I);
}

int randInt(int pMin, int pMax){
    return (rand() % (pMax - pMin + 1)) + pMin;
}

int main(){
    srand(time(NULL));
    struct Matroid* intersectionMatroids[INTERSECTIONS_TOTAL];

    //Prime matroid
    printf("Prime matroid\n");
    testMatroid(0, 10, &isPrime);


    //Even matroid
    printf("\nEven matroid\n");
    testMatroid(10, 20, &isEven);


    //Odd matroid
    printf("\nOdd matroid\n");
    testMatroid(20, 30, &isOdd);


    //matroids intersection
    printf("\nMatroids intersection\n\n");
    struct VectorOfPointers* S;
    struct VectorOfPointers* I;
    MatroidW* W = &isEven;
    struct Matroid* matroid;
    for (int index = 0; index < INTERSECTIONS_TOTAL; index++) {
        S = newVectorOfPointers();
        I = newVectorOfPointers();
        for (int i = 0; i < 10; i++) {
            pushPointer(S, newInt(randInt(0,5)));
        }
        matroid = createMatroid(S, I);
        intersectionMatroids[index] = matroid;
        solveMatroid(matroid, W);
        printIntMatroid(matroid);
        printf("\n");
    }
    struct VectorOfPointers* intersections = intersectIntMatroids(intersectionMatroids, INTERSECTIONS_TOTAL);
    printf("Intersections: %d\nElements: ", intersections->lenght);
    for (int element = 0; element < intersections->lenght; element++) {
        printf("%d ", *(int*)intersections->pointers[element]);
    }
    printf("\n");
    return 0;
}
