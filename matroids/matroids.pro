TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -fopenmp
QMAKE_LFLAGS += -fopenmp
QMAKE_CXXFLAGS += -fopenmp
QMAKE_CFLAGS_DEBUG += -fopenmp

SOURCES += \
    functions.c \
        main.c \
    Matroid.c \
    vectorOfPointers.c

HEADERS += \
    Matroid.h \
    functions.h \
    vectorOfPointers.h
