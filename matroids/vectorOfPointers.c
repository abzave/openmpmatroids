#include "vectorOfPointers.h"

struct VectorOfPointers* newVectorOfPointers(){
    struct VectorOfPointers* new = malloc(sizeof (struct VectorOfPointers));
    new->pointers = malloc(sizeof (void*));
    new->lenght = 0;
    return new;
}


void pushPointer(struct VectorOfPointers* pVector, void* pPointer){
    pVector->pointers = realloc(pVector->pointers, (pVector->lenght+1) * sizeof(void*));
    pVector->pointers[pVector->lenght++] = pPointer;
}

struct VectorOfPointers* intersectIntVectors(struct VectorOfPointers* pVector1, struct VectorOfPointers* pVector2){
    struct VectorOfPointers* intersection = newVectorOfPointers();
    #pragma omp parallel for
    for(int vector1Index = 0; vector1Index < pVector1->lenght; vector1Index++) {
        void** vector1 = pVector1->pointers;
        #pragma omp parallel for
        for(int vector2Index = 0; vector2Index < pVector2->lenght; vector2Index++) {
            void** vector2 = pVector2->pointers;
            if((*(int*)vector1[vector1Index]) == (*(int*)vector2[vector2Index])){
                char intersectionHasValue = 0;
                int value = *(int*)vector1[vector1Index];
                for (int element = 0; element < intersection->lenght; element++) {
                    if(value == *(int*)intersection->pointers[element]){
                        intersectionHasValue = 1;
                    }
                }
                if(!intersectionHasValue){
                    #pragma omp critical
                    pushPointer(intersection, vector1[vector1Index]);
                }
            }
        }
    }
    return intersection;
}
