#ifndef VECTOROFPOINTERS_H
#define VECTOROFPOINTERS_H


struct VectorOfPointers{
  void** pointers;
  int lenght;
};


void pushPointer(struct VectorOfPointers* pVector, void* pPointer);
struct VectorOfPointers* newVectorOfPointers(void);
struct VectorOfPointers* intersectIntVectors(struct VectorOfPointers* pVector1, struct VectorOfPointers* pVector2);
#endif // VECTOROFPOINTERS_H
